#include "Utils.h"

bool Utils::loadConfig(map<string, string>& props)
{
	MString result;
	CHECK_MSTATUS(MGlobal::executeCommand("internalVar-usd", result));
	result += "pEnginePlugin/pEngineConfig.pCfg";
	string b = result.asChar();
	replace(b.begin(), b.end(), '\\', '/');

	ifstream f(b);
	if (!f)
	{
		MStreamUtils::stdErrorStream() << "Error load Progressive config file!\n";
		return false;
	}

	while (true)
	{
		string tmp;
		getline(f, tmp, '\n');
		if (!f)
		{
			MStreamUtils::stdErrorStream() << "Error read Progressive config file!\n";
			return false;
		}
		int index = tmp.find(";");
		if (index != -1)
			tmp = tmp.erase(index);
		regex regular("\\s+");
		vector<string> results;
		for_each(sregex_token_iterator(tmp.begin(), tmp.end(), regular, -1), sregex_token_iterator(), [&results](const string& data)
			{
				string newLine = regex_replace(data, regex("^ +| +$|( ) +"), "");
				//newLine = regex_replace(data, regex("[-.?!)(,:;]"), "");
				results.push_back(newLine);
			}
		);
		if (results.size() < 3)
			continue;
		if (props.find(results[0]) != props.end())
		{
			string line;
			for (int i(2); i < results.size(); i++)
				line += results[i] + " ";
			line.resize(line.length() - 1);
			props[results[0]] = line;
			replace(props[results[0]].begin(), props[results[0]].end(), '\\', '/');
		}
		if (f.eof())
			return true;
	}

	f.close();
	return true;
}

bool Utils::loadMelScript(string& script)
{
	MString result;
	CHECK_MSTATUS(MGlobal::executeCommand("internalVar-usd", result));
	result += "pEnginePlugin/pEngineGlobalVariables.mel";
	string b = result.asChar();
	replace(b.begin(), b.end(), '\\', '/');

	ifstream f(b);
	if (!f)
	{
		MStreamUtils::stdErrorStream() << "Error load pEngineGlobalVariables script file!\n";
		return false;
	}

	string tmp;
	while (true)
	{
		getline(f, tmp, '\n');
		if (!f)
		{
			MStreamUtils::stdErrorStream() << "Error load pEngineGlobalVariables script file!\n";
			return false;
		}
		regex regular("\\s+");
		vector<string> results;
		for_each(sregex_token_iterator(tmp.begin(), tmp.end(), regular, -1), sregex_token_iterator(), [&results](const string& data)
			{
				string newLine = regex_replace(data, regex("^ +| +$|( ) +"), "");
				newLine = regex_replace(data, regex("[-.?!)(,:;]"), "");
				results.push_back(newLine);
			}
		);
		if (results.size() != 3)
		{
			MStreamUtils::stdErrorStream() << "Error load pEngineGlobalVariables script file!\n";
			return false;
		}
		script = results[2];
		break;
	}
	f.close();
	return true;
}
