#include "progressivePlugin.h"

//////////////////////////////////////////////////////////////

const char * const pObjectPlugin::optionScript("progressiveExportOptions");
const char * const pObjectPlugin::optionScriptDefaultValues("");

//////////////////////////////////////////////////////////////

bool pObjectPlugin::isInit(true);
map<string, string> pObjectPlugin::props;

pObjectPlugin::pObjectPlugin()
{
	MStreamUtils::stdErrorStream() << "Progressive config file load succesful.\n";
	if (!Utils::loadMelScript(nameLogVar))
		return;
	MStreamUtils::stdErrorStream() << "pEngineGlobalVariables script file load succesful.\n";
	isInit = true;
	pExt = "pObject";
}

//////////////////////////////////////////////////////////////

void* pObjectPlugin::creator()
{
    return new pObjectPlugin();
}

//////////////////////////////////////////////////////////////
MString pObjectPlugin::defaultExtension() const
{
	return MString(pExt.c_str());
}

//////////////////////////////////////////////////////////////

MStatus pObjectPlugin::reader ( const MFileObject& file,
                                const MString& options,
                                FileAccessMode mode)
{
    fprintf(stderr, "ObjTranslator::reader called in error\n");
    return MS::kFailure;
}

//////////////////////////////////////////////////////////////

MStatus pObjectPlugin::writer ( const MFileObject& file,
                                const MString& options,
                                FileAccessMode mode )
{
	if (!isInit)
	{
		MStreamUtils::stdErrorStream() << "pEngine plugin initialize error! Please, reinstall plugin. \n";
		return MStatus::kFailure;
	}

	printMessageToLog("Range Progressive Engine. Exporting model(s) from maya.");
	
	string opt = options.asChar();
	if (opt == ";") 
	{
		MStreamUtils::stdErrorStream() << "Your export settings are not correct. Please use the \"pEngineTools\" export shell. \n";
		return MStatus::kFailure;
	}

	MStatus status = MStatus::kSuccess;  // статус выполнения методов
    
	// имя файла
    MString mname = file.fullName();

	if ((mode == MPxFileTranslator::kExportAccessMode) ||
		(mode == MPxFileTranslator::kSaveAccessMode)) // если мы выбрали опцию экспортировать всё( или сохранить всё? )
	{
		status = exportAll(); // TODO: реализовать
		clear();
		return MS::kFailure;
	}
    else if( mode == MPxFileTranslator::kExportActiveAccessMode ) // если выбрали опцию экспортировать выделенные
        status = exportSelection();

	// TODO: провреить, как можно еще оптимизировать количество вершин(в maya 2000, в движке 3500)

	if (status != MStatus::kSuccess)
		return status;

	// создаем файл
	inputFile.open(mname.asChar(), std::ios_base::binary);
	if (!inputFile) // если файл нельзя создать(права доступа и т.д.)
	{
		string out = "Error: The file ";
		out += mname.asChar();
		out += " could not be opened for writing.";
		printMessageToLog(out);
		clear();
		return MS::kFailure;
	}

	if (!writeInfoFromFile()) // пишем служебную информацию
	{
		string out = "Error write data from ";
		out += mname.asChar();
		out += " file.";
		printMessageToLog(out);
		clear();
		return MStatus::kFailure;
	}
	if (!writeMaterialsFromFile()) // записываем все материалы в файл
	{
		string out = "Error write data from ";
		out += mname.asChar();
		out += " file.";
		printMessageToLog(out);
		clear();
		return MStatus::kFailure;
	}
	if (!writeMeshesFromFile()) // пишем все меши в файл
	{
		string out = "Error write data from ";
		out += mname.asChar();
		out += " file.";
		printMessageToLog(out);
		clear();
		return MStatus::kFailure;
	}
	if (!writeVertexsFromFile()) // записываем все вершины в файл
	{
		string out = "Error write data from ";
		out += mname.asChar();
		out += " file.";
		printMessageToLog(out);
		clear();
		return MStatus::kFailure;
	}
	if (!writeOptionsFromFile(mname, options)) // записываем настройки в файл
	{
		clear();
		return MStatus::kFailure;
	}
	
	string out = "Model ";
	out += mname.asChar();
	out += " export successeful.";
	printMessageToLog(out);
	printMessageToLog("---------------------------");
	clear(); // очищаем все буферы
	return MStatus::kSuccess;
}
//////////////////////////////////////////////////////////////

void pObjectPlugin::setToLongUnitName(const MDistance::Unit &unit, MString& unitName)
{
    switch( unit ) 
	{
	case MDistance::kInches:
		/// Inches
		unitName = "inches";
		break;
	case MDistance::kFeet:
		/// Feet
		unitName = "feet";
		break;
	case MDistance::kYards:
		/// Yards
		unitName = "yards";
		break;
	case MDistance::kMiles:
		/// Miles
		unitName = "miles";
		break;
	case MDistance::kMillimeters:
		/// Millimeters
		unitName = "millimeters";
		break;
	case MDistance::kCentimeters:
		/// Centimeters
		unitName = "centimeters";
		break;
	case MDistance::kKilometers:
		/// Kilometers
		unitName = "kilometers";
		break;
	case MDistance::kMeters:
		/// Meters
		unitName = "meters";
		break;
	default:
		break;
	}
}
//////////////////////////////////////////////////////////////

bool pObjectPlugin::haveReadMethod () const
{
    return false;
}
//////////////////////////////////////////////////////////////

bool pObjectPlugin::haveWriteMethod () const
{
    return true;
}

//////////////////////////////////////////////////////////////
MPxFileTranslator::MFileKind pObjectPlugin::identifyFile(
	const MFileObject& fileName,
	const char* buffer,
	short size) const
{
	const char * name = fileName.name().asChar();
	int   nameLength = strlen(name);

	if ((nameLength > pExt.length()) && !strcasecmp(name + nameLength - pExt.length(), pExt.c_str()))
		return kCouldBeMyFileType;
	else
		return kNotMyFileType;
}


//////////////////////////////////////////////////////////////

MStatus pObjectPlugin::OutputPolygons(MDagPath& mdagPath)
{
	// главный метод получения вершин, нормалей, текстурных координат

	// временные переменные
	MStatus stat = MS::kSuccess; // статус выполнения метода
	int instanceNum(0);
	MObjectArray sets;
	MObjectArray comps;

	// получаем массив материалов для этого меша
	if (mdagPath.isInstanced())
		instanceNum = mdagPath.instanceNumber();

	MFnMesh fnMesh(mdagPath, &stat);
	if (stat != MS::kSuccess)
	{
		printMessageToLog("Failure in MFnMesh initialization.");
		return MS::kFailure;
	}

	if (!fnMesh.getConnectedSetsAndMembers(instanceNum, sets, comps, true))
	{
		printMessageToLog("ERROR: MFnMesh::getConnectedSetsAndMembers");
		return MS::kFailure;
	}

	// перебираем все материалы, и записывем данные полигонов, которые соответсвуют конкретному материалу
	string nameMesh = fnMesh.name().asChar();
	meshesName.push_back(nameMesh);
	meshesIndeces.push_back(0);
	for (unsigned i = 0; i < sets.length(); i++)
	{
		MStatus stat = writePolyFromBuffer(fnMesh, mdagPath, sets[i], comps[i]);
		if (stat == MStatus::kNotImplemented)
			continue;
		else if (stat == MStatus::kFailure)
			return stat;
		meshesIndeces.back()++;
	}

	return MStatus::kSuccess;
}

//////////////////////////////////////////////////////////////

MStatus pObjectPlugin::exportSelection( )
{
	MStatus status;
	MString filename;

	MSelectionList slist;  // список объектов
	MGlobal::getActiveSelectionList( slist ); // получаем список выделенных объектов в maya
	MItSelectionList iter( slist ); // создаем итератор на первый элемент из выделенных объектов

	if (iter.isDone())  // если следующего элемента нет(если список пуст)
	{
		printMessageToLog("Error: Nothing is selected.");
	    return MS::kFailure; // возвращаем ошибку
	}

    // инициализируем корневой итератор, для обхода группы объектов
	// 1 - сначала обходим элементы по вертикали
	// 2 - фильтрация отключена(неизвестный параметр)
	// 3 - статус создания итератора
    MItDag dagIterator( MItDag::kDepthFirst, MFn::kInvalid, &status);

	// в цикле выбираем все выделенные модели
	for ( ; !iter.isDone(); iter.next() )
	{	 
		MDagPath objectPath; // хранит подобъект из группы объектов
		status = iter.getDagPath( objectPath); // получаем первый подобъект из текущего объекта

		//  сбрасываем корневой итератор на начальный узел
		status = dagIterator.reset (objectPath.node(), 
									MItDag::kDepthFirst, MFn::kInvalid );	

		// Перебираем все подобъекты из одного объекта
		for ( ; !dagIterator.isDone(); dagIterator.next() )
		{
			MDagPath dagPath; // хранит указанный узел из объекта(подобъект)
			status = dagIterator.getPath(dagPath); // получаем текущий узел

			if (!status) // узел не корректный
			{ 
				printMessageToLog("Failure getting DAG path.");
				return MS::kFailure;
			}

			if (status) // объект корректен
			{
                MFnDagNode dagNode( dagPath, &status ); // набор функций с узлом
                if (dagNode.isIntermediateObject())  // если объект промежуточный(для транформаций и т.д.)
                    continue; // не обрабатываем его

				// возвращает истину, если заданный набор функций поддерживается подобъектом
				if (dagPath.hasFn(MFn::kNurbsSurface))  // если подобъект поддерживает NURBS примитивы
				{
					status = MS::kSuccess;
					printMessageToLog("Warning: skipping Nurbs Surface.");
				}
				else if ((  dagPath.hasFn(MFn::kMesh)) && (  dagPath.hasFn(MFn::kTransform))) // если это меш и трансформация - то выходим из обработки цикла
					continue;
				else if (  dagPath.hasFn(MFn::kMesh)) // если это просто меш - то его обрабатываем
				{
					status = OutputPolygons(dagPath); // построение полигонов
					if (status != MS::kSuccess) // ошибка записи данных о модели
					{
						printMessageToLog("Error: exporting geom failed, check your selection.");
						return status;
					}
				}
			}
		}
	}
		
	return status;
}

//////////////////////////////////////////////////////////////
MFnDagNode pObjectPlugin::get_object_by_name(const char* name)
{
	MStatus status;
	MItDag dagIter(MItDag::kDepthFirst, MFn::kInvalid, &status);
	for (; !dagIter.isDone(); dagIter.next()) {
		MDagPath dagPath;
		status = dagIter.getPath(dagPath);

		MFnDagNode nodeFn;
		nodeFn.setObject(dagPath);
		if (strcmp(nodeFn.name().asChar(), name) == 0)
		{
			return dagPath;
		}
	}
	return MDagPath();
}


MStatus pObjectPlugin::exportAll( )
{
	MStatus status = MS::kSuccess;
	int count_model(0);
	vector<MObject> vecObj;

	MItDag dagIterator( MItDag::kBreadthFirst, MFn::kInvalid, &status);

	if ( MS::kSuccess != status)
	{
		printMessageToLog("Failure in DAG iterator setup.");
	    return MS::kFailure;
	}

	for ( ; !dagIterator.isDone(); dagIterator.next() )
	{
		MDagPath dagPath;
		MObject  component = MObject::kNullObj;
		status = dagIterator.getPath(dagPath);

		if (!status) 
		{
			printMessageToLog("Failure getting DAG path.");
			return MS::kFailure;
		}

		MFnDagNode dagNode( dagPath, &status );
		if (dagNode.isIntermediateObject()) 
		{
			continue;
		}

		if ((  dagPath.hasFn(MFn::kNurbsSurface)) &&
			(  dagPath.hasFn(MFn::kTransform)))
		{
			status = MS::kSuccess;
			printMessageToLog("Warning: skipping Nurbs Surface.");
		}
		else if ((  dagPath.hasFn(MFn::kMesh)) &&
				 (  dagPath.hasFn(MFn::kTransform)))
		{
			continue;
		}
		else if (  dagPath.hasFn(MFn::kMesh))
		{			
			MObject obj(dagPath.node());
			vecObj.push_back(obj);
			count_model++;
			
			/*status = OutputPolygons(dagPath, component);
			if (status != MS::kSuccess) {
				fprintf(stderr,"Error: exporting geom failed.\n");
                return MS::kFailure;
            }*/
		}
	}

	// ТЕСТЫ!!!!!!!

	//MObjectArray arr(vecObj.size());
	//for (int i(0); i < arr.length(); i++)
	//	arr[i] = vecObj[i];

	//MFnRenderLayer* renderLayer = new MFnRenderLayer();

	//MHWRender::MRenderer* renderer = MHWRender::MRenderer::theRenderer();
	//
	//// создали красное изображение 512 x 512 и сохранили на диск
	//MImage img;
	//unsigned char pixels[512 * 515 * 4];
	//for (int i = 0; i < 512 * 515 * 4; i += 4)
	//{
	//	pixels[i] = pixels[i + 3] = (char)255;
	//	pixels[i + 1] = pixels[i + 2] = (char)0;
	//}
	//img.setPixels(pixels, 512, 512);
	//img.writeToFile(R"(C:\Users\admin_test\Desktop\test.jpg)", "jpg");
	//

	//M3dView m3d = M3dView::active3dView(&status);
	//if (status != MStatus::kSuccess)
	//	return status;
	//MImage img;

	//// create camera - ok !
	//MDagModifier dagModifier;
	//MObject cameraTransformObj = dagModifier.createNode("transform");
	//dagModifier.renameNode(cameraTransformObj, "myCameraTransform");
	//MFnTransform transformFn(cameraTransformObj);
	//transformFn.setTranslation(MVector(0, 0, 40), MSpace::kTransform);

	//MObject cameraShapeObj = dagModifier.createNode("camera", cameraTransformObj);
	//dagModifier.renameNode(cameraShapeObj, "myCameraShape");
	//dagModifier.doIt();

	//MDagPath cam;
	//if (cameraShapeObj.hasFn(MFn::kDagNode))
	//{
	//	cam = MDagPath::getAPathTo(cameraShapeObj);
	//	m3d.setCamera(cam);
	//	m3d.refresh(); // как - то вызвать обновление вьюпорта
	//}
	//else
	//	return MStatus::kFailure;
	////






	// получаем камеру перспективы
	//MDagPath dag = get_object_by_name("persp").dagPath();
	//MFnTransform trans(dag);
	//// получаем позицию и перемещаем в начало координат
	//MVector pos = trans.getTranslation(MSpace::kTransform);
	//trans.setTranslation(MVector(-pos.x, -pos.y, -pos.z), MSpace::kTransform);
	//// получаем поворот и вращаем на 0
	//MEulerRotation rotate;
	//trans.getRotation(rotate);
	//MEulerRotation newRotate(rotate);
	//newRotate.x = -newRotate.x;
	//newRotate.y = -newRotate.y;
	//newRotate.z = -newRotate.z;
	// перемещаем камеру на 40 единиц назад по z
	//trans.setTranslation(MVector(0, 0, -40), MSpace::kTransform);



	//// запоминаем старую камеру
	//MDagPath cameraOldPath;
	//m3d.getCamera(cameraOldPath);

	//m3d.setObjectDisplay(M3dView::DisplayObjects::kDisplayMeshes);
	//m3d.refresh(true, true);

	/*m3d.readColorBuffer(img, true);

	status = img.writeToFile(R"(C:\Users\admin_test\Desktop\test.jpg)", "jpg");
	if (status != MStatus::kSuccess)
		return status;*/

	return status;
}


bool pObjectPlugin::isVisible(MFnDagNode & fnDag, MStatus& status)
{
	/*if (fnDag.isIntermediateObject()) 
		return false;*/

	MPlug visPlug = fnDag.findPlug("visibility", &status); // зарашиваем состояние видимости
	if (MStatus::kFailure == status)  // не удалось запросить состояние
	{
		printMessageToLog("MPlug::findPlug error!");
		//MGlobal::displayError("MPlug::findPlug");
		return false;
	}
	else 
	{
		bool visible;
		status = visPlug.getValue(visible); // получаем значение видимости
		if (MStatus::kFailure == status)
		{
			//MGlobal::displayError("MPlug::getValue");
			printMessageToLog("MPlug::getValue error!");
			return false;
		}
		return visible;
	}
}


MStatus pObjectPlugin::writePolyFromBuffer(MFnMesh& fnMesh, 
	MDagPath& mdagPath, 
	MObject& set,
	MObject& comp)
{
	// переменные
	MStatus stat = MStatus::kSuccess; // статус выполнения метода
	MSpace::Space space = MSpace::kWorld; // координаты(мировые)
	Material mat; // создаём материал
	Color colorModel;
	wstring tmpIsColor(IS_COLOR);
	string isColor(tmpIsColor.begin(), tmpIsColor.end());
	
	string texturesPath = props["$game_textures"];

	MFnSet fnSet(set, &stat); // создаем набор
	if (stat == MS::kFailure)   // не удалось создать
	{
		MStreamUtils::stdErrorStream() << "ERROR: MFnSet::MFnSet\n";
		return MStatus::kNotImplemented;
	}

	MItMeshPolygon polyIter(mdagPath, comp, &stat); // итератор на вершины
	if ((stat == MS::kFailure)/* || comp.isNull()*/) // не удалось создать итератор
	{
		MStreamUtils::stdErrorStream() << "ERROR: Can't create poly iterator!\n";
		return MStatus::kNotImplemented;
	}

	MFnDependencyNode fnNode(set); // получаем материал
	MPlug shaderPlug = fnNode.findPlug("surfaceShader");
	MPlugArray connectedPlugs;
	if (shaderPlug.isNull())
	{
		MStreamUtils::stdErrorStream() << "ERROR: Can't find material!\n";
		return MStatus::kNotImplemented;
	}
	shaderPlug.connectedTo(connectedPlugs, true, false);

	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MFnDependencyNode fnDN(connectedPlugs[0].node()); // получаем имя материала
	mat.nameMaterial = fnDN.name().asChar(); // запоминаем имя материала

	MObject pEngineMaterial = fnDN.attribute("PEngineMaterial", &stat);
	if (stat == MS::kInvalidParameter)
	{
		MStreamUtils::stdErrorStream() << "ERROR: can't find pEngine material\n";
		return MStatus::kNotImplemented;
	}
	int indexMaterial = MPlug(fnDN.object(), pEngineMaterial).asInt(); // get pEngine material
	eAttr.setObject(pEngineMaterial);
	MString mName = eAttr.fieldName(indexMaterial, &stat);
	mat.valueMaterial= mName.asChar();
	if (stat != MS::kSuccess)
	{
		MStreamUtils::stdErrorStream() << "ERROR: can't get pEngine material\n";
		return MStatus::kNotImplemented;
	}

	MObject pEngineDoubleSides= fnDN.attribute("PDoubleSides", &stat);  // get pEngine double sides
	if (stat == MS::kInvalidParameter)
	{
		MStreamUtils::stdErrorStream() << "ERROR: debug\n";
		return MStatus::kNotImplemented;
	}
	nAttr.setObject(pEngineDoubleSides);
	mat.doubleSides = static_cast<bool>(MPlug(fnDN.object(), pEngineDoubleSides).asInt());
	

	MItDependencyGraph dgIt(shaderPlug, MFn::kFileTexture, // получаем граф текстур
		MItDependencyGraph::kUpstream,
		MItDependencyGraph::kBreadthFirst,
		MItDependencyGraph::kNodeLevel,
		&stat);
	if (stat == MS::kFailure) //не смогли получить граф текстур
	{
		MStreamUtils::stdErrorStream() << "ERROR: Can't load graph textures\n";
		return MStatus::kNotImplemented;
	}
	dgIt.disablePruningOnFilter();

	if (!dgIt.isDone()) // текстуры имеются
	{
		MObject textureNode = dgIt.thisNode(); // получаем имя текстур
		MPlug filenamePlug = MFnDependencyNode(textureNode).findPlug("fileTextureName");
		MString textureName; // имя текстуры
		filenamePlug.getValue(textureName);
		mat.nameTexture = textureName.asChar(); // запоминаем имя текстуры
		if (mat.nameTexture.find(texturesPath) != string::npos)
			mat.nameTexture = mat.nameTexture.erase(0, texturesPath.length() + 1);
		else
		{
			int indexSlash = mat.nameTexture.rfind("/");
			mat.nameTexture = mat.nameTexture.erase(0, indexSlash + 1);
		}
	}
	else // текстур нет
	{
		vector< float> color(4);
		vector< MString> colorNames = { "colorR", "colorG", "colorB", "transparencyR" };
		for (size_t i(0); i < color.size(); i++)
		{
			auto cl = fnDN.attribute(colorNames[i], &stat);  // get pEngine double sides
			if (stat == MS::kFailure)
			{
				MStreamUtils::stdErrorStream() << "ERROR: Can't find " << colorNames[i] << " \n";
				return MStatus::kNotImplemented;
			}
			color[i] = MPlug(fnDN.object(), cl).asFloat(); // get part color
		}
		colorModel = { color[0], color[1] ,color[2] ,color[3] };
		mat.nameTexture = isColor;
	}

	// получаем индекс материала
	indexMaterial = -1; // индекс материала для буфера индексов
	if (bufMaterials.find(mat) != bufMaterials.end())
		indexMaterial = bufMaterials[mat];
	else
	{
		int value(bufMaterials.size());
		bufMaterials.insert({ mat, value });
		indexMaterial = value;
	}
	// получаем индексный буфер, который соответствует конкретному материалу
	vector<Index>* indeces;
	if (arrayIndeces.find(indexMaterial) == arrayIndeces.end())
		arrayIndeces.insert({ indexMaterial, {} });
	indeces = &arrayIndeces[indexMaterial];

	 // здесь перебираются все полигоны, которые принадлежат этому материалу и все их данные записываются в один буфер(потом запишем буфер в файл)
	for (; !polyIter.isDone(); polyIter.next())
	{
		int polyVertexCount = polyIter.polygonVertexCount(); // получаем количество вершин в полигоне
		if (polyVertexCount != 3)  // проверка, триангулирована ли модель
		{
			MStreamUtils::stdErrorStream() << "Model " << fnMesh.name().asChar() << " is not triangulate!\n";
			return MS::kFailure;
		}
		for (int vtx = 0; vtx < polyVertexCount; vtx++) // перебираем все вершины в полигоне
		{
			// переменные
			Vertex vertex;

			// получаем вершину
			MPointArray vert;
			polyIter.getPoints(vert, space);
			vertex.pos = { (float)vert[vtx].x, (float)vert[vtx].y, (float)vert[vtx].z }; // z - координату отзеркаливаем для D3D | -z

			// получаем текстурные координаты вершины
			MFloatArray us;
			MFloatArray vs;
			polyIter.getUVs(us, vs);
			if(mat.nameTexture != isColor)
				vertex.color = { us[vtx] , vs[vtx] , 0.f, -1.f }; // меняем координату v для D3D | 0 - v
			else
				vertex.color = colorModel;

			// получаем нормаль
			MVectorArray norm;
			polyIter.getNormals(norm, space);
			vertex.normals = { (float)norm[vtx].x, (float)norm[vtx].y, (float)norm[vtx].z }; // | -z

			// конструируем массив вершин и индексов
			auto iter = arrayVertex.find(vertex);
			Index indexVertex(0);
			if (iter == arrayVertex.end()) // если такой вершины нет
			{
				indexVertex = arrayVertex.size();
				arrayVertex.insert({ vertex, indexVertex }); // добавили вершину
			}
			else
				indexVertex = iter->second;

			// добавляем индекс
			indeces->push_back(indexVertex);

			// конструируем массив нормалей для усреднения
			if (arrayNormal.find(vertex) == arrayNormal.end()) // если нет такой вершины
			{
				std::set<Vector3> s;
				s.insert(vertex.normals);
				arrayNormal.insert(std::make_pair(vertex, s));
			}
			else
				arrayNormal[vertex].insert(vertex.normals);

		}
		// std::swap(indeces->back(), indeces->at(indeces->size() - 3)); // переворачиваем треугольник(чтобы в движке был лицом к камере)
	}

	return MStatus::kSuccess;
}

//////////////////////////////////////////////////////////////
bool pObjectPlugin::writeInfoFromFile()
{
	// пишем в буфер номер блока - блок служебной информации
	int block = pObjFormatBlocks::serviceInfo;
	inputFile.write((char*)&block, sizeInt);
	if (!inputFile)
		return false;
	// пишем в буфер данные о пользователе, который создал эту модель
	size_t sizeBuffer;
	errno_t error = getenv_s(&sizeBuffer, nullptr, 0, "USERNAME");
	if(error != 0)
		return false;
	string name; name.resize(sizeBuffer);
	error = getenv_s(&sizeBuffer, (char*)name.c_str(), name.size(), "USERNAME");
	if (error != 0)
		return false;
	writeLineFromFile(name.c_str());
	// пишем в буфер данные о дате и времени создания модели
	tm tim;
	time_t tt = time(NULL);
	error = localtime_s(&tim , &tt);
	if (error != 0)
		return false;
	DateTime dt(tim.tm_sec, tim.tm_min, tim.tm_hour, tim.tm_mday, tim.tm_mon, tim.tm_year);
	inputFile.write((char*)&dt, sizeDataTime);
	if (!inputFile)
		return false;
	// пишем в файл данные о дате и времени модификации модели(записывается в редакторе)
	inputFile.write((char*)&dt, sizeDataTime);
	if (!inputFile)
		return false;
	// пишем в файл количество мешей в модели
	MSelectionList slist;  // список объектов
	MGlobal::getActiveSelectionList(slist); // заполняем список объектов выделенными(если export all - то выделены все)
	int length(slist.length());
	inputFile.write((char*)&length, sizeInt);
	if (!inputFile)
		return false;
	// пишем в буфер центр модели(общий)
	//vector<MPoint> centers;
	vector< MBoundingBox> bboxArr;
	for (MItSelectionList iter(slist); !iter.isDone(); iter.next())
	{
		MDagPath path;
		iter.getDagPath(path);
		MFnMesh fnMesh(path); // данные о меше
		MBoundingBox bbox = fnMesh.boundingBox(); // ограничивающий бокс
		//centers.push_back(bbox.center()); // запоминаем центр
		bboxArr.push_back(bbox); // запоминаем bbox
	}
	// рассчитываем общий центр
	MPoint center; // рассчитанный центр
	float xl(bboxArr[0].min().x), xr(bboxArr[0].max().x), yd(bboxArr[0].min().y), yu(bboxArr[0].max().y), zr(-bboxArr[0].min().z), zf(-bboxArr[0].max().z); // границы огранич. объема
	if (bboxArr.size() == 1)
	{
		center = bboxArr[0].center();
		center.z = -center.z;
	}
	else
	{
		for (auto&bb : bboxArr)
		{
			auto p = bb.min(); p.z = -p.z;
			auto p1 = bb.max(); p1.z = -p1.z;
			if (xl > p.x)
				xl = p.x;
			if (xr < p1.x)
				xr = p1.x;
			if (yd > p.y)
				yd = p.y;
			if (yu < p1.y)
				yu = p1.y;
			if (zr > p.z)
				zr = p.z;
			if (zf < p1.z)
				zf = p1.z;
		}
		center = { (xr - xl) / 2, (yu - yd) / 2, (zf - zr) / 2 };
	}
	float cx = center.x;
	inputFile.write((char*)&cx, sizeFloat);
	if (!inputFile)
		return false;
	float cy = center.y;
	inputFile.write((char*)&cy, sizeFloat);
	if (!inputFile)
		return false;
	float cz = center.z;
	inputFile.write((char*)&cz, sizeFloat);
	if (!inputFile)
		return false;
	// пишем в буфер крайние координаты BBox(движок сам построит модель)
	//xl--; yd--; zf--; xr++; yu++; zr++;
	inputFile.write((char*)&xl, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&yd, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&zf, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&xr, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&yu, sizeFloat);
	if (!inputFile)
		return false;
	inputFile.write((char*)&zr, sizeFloat);
	if (!inputFile)
		return false;
	// записываем единицы измерения в первый блок
	MString unitName;
	setToLongUnitName(MDistance::uiUnit(), unitName);
	if (!writeLineFromFile(unitName.asChar()))
		return false;

	string out = "Write specified information write: succesfull.";
	printMessageToLog(out);
	return true;
}

//////////////////////////////////////////////////////////////
bool pObjectPlugin::writeMaterialsFromFile()
{
	int block = pObjFormatBlocks::materialsInfo;
	inputFile.write((char*)&block, sizeInt ); // пишем сначала блок(главный)
	if (!inputFile)
		return false;
	int s = bufMaterials.size();
	inputFile.write((char*)&s, sizeInt); // пишем кол-во материалов
	if (!inputFile)
		return false;
	for (auto& t : bufMaterials)
	{
		tmpIndecesMaterialBuffer.push_back(t.second);
		if (!inputFile)
			return false;
		if (!writeLineFromFile(t.first.nameMaterial.c_str())) // пишем название материала
			return false;
		if (!writeLineFromFile(t.first.nameTexture.c_str())) // пишем путь к текстуре
			return false;
		if (!writeLineFromFile(t.first.valueMaterial.c_str())) // пишем материал
			return false;
		inputFile.write((char*)&t.first.doubleSides, sizeBoolean); // пишем дадл сайдс
		if (!inputFile)
			return false;
	}
	string out = "Wtite materials:  " + to_string(s) + ". Succesfull.";
	printMessageToLog(out);
	return true;
}

//////////////////////////////////////////////////////////////
bool pObjectPlugin::writeLineFromFile(const char* line)
{
	for (int i(0); i < strlen(line); i++)
	{
		inputFile.write((char*)&line[i], sizeChar); // пишем по символу
		if (!inputFile)
			return false;
	}
	char null('\0');
	inputFile.write((char*)&null, sizeChar); // пишем нуль - терминал
	if (!inputFile)
		return false;
	return true;
}

//////////////////////////////////////////////////////////////

bool pObjectPlugin::writeVertexsFromFile()
{
	int block = pObjFormatBlocks::vertexInfo;
	inputFile.write((char*)&block, sizeInt ); // пишем сначала блок(главный)
	if (!inputFile)
		return false;
	int s = arrayVertex.size();
	inputFile.write((char*)&s, sizeInt); // количество вершин
	if (!inputFile)
		return false;
	map<int, Vertex> vertexWrited;
	for_each(arrayVertex.begin(), arrayVertex.end(), [&vertexWrited](const auto& v)
		{
			vertexWrited.insert({ v.second, v.first });
		}
	);
	for (int i(0); i < vertexWrited.size(); i++)
	{
		auto v = vertexWrited[i]; // получаем вершину
		auto n = arrayNormal[v];

		Vector3 n_result;
		for (auto& _n : n)
			n_result += _n;
		v.normals = n_result * (1.f / n.size());
		
		//v.normal = n.normal * (1.f /  n.count); // рассчитываем ей нормаль

		//ver.normal.z = -ver.normal.z;
		
		inputFile.write((char*)&v, sizeVertex); // буфер вершин // v
		if (!inputFile)
			return false;
	}
	string out = "Write vertexs:  " + to_string(s) + ". Successfull";
	printMessageToLog(out);
	return true;
}

//////////////////////////////////////////////////////////////

bool pObjectPlugin::writeMeshesFromFile()
{
	int block = pObjFormatBlocks::meshesInfo;
	int blockMesh = block * 10;
	int blockIndecesBuffer;
	int allIndexs(0);

	inputFile.write((char*)&block, sizeInt ); // пишем сначала блок(главный)
	if (!inputFile)
		return false;
	for (int k(0), i(0); k < meshesName.size(); k++)
	{
		// пишем id подблока
		inputFile.write((char*)&blockMesh, sizeInt); // пишем сначала блок
		if (!inputFile)
			return false;
		blockMesh += 0x0001; // приращиваем номер блока
		blockIndecesBuffer = block * 1000; // обнуляем номер блока для индексных буферов

		inputFile.write((char*)&meshesIndeces[k], sizeInt); // пишем количество индексных буферов для меша
		if (!inputFile)
			return false;
		//Пишем в буфер по порядку: имя меша
		if (!writeLineFromFile(meshesName[k].c_str())) // имя меша
			return false;

		for (int l(0); l < meshesIndeces[k]; l++)
		{
			// пишем id подблока
			inputFile.write((char*)&blockIndecesBuffer, sizeInt); // пишем сначала блок
			if (!inputFile)
				return false;

			int indexIndeces = tmpIndecesMaterialBuffer[i++];
			blockIndecesBuffer += 0x0001; // приращиваем номер блока

			// пишем в буфер кол-во индексов, буфер индексов и так все буферы индексов
			int s = arrayIndeces[indexIndeces].size();
			inputFile.write((char*)&s, sizeInt); // количество индексов
			if (!inputFile)
				return false;
			allIndexs += arrayIndeces[indexIndeces].size();
			for (auto& ind : arrayIndeces[indexIndeces])
			{
				inputFile.write((char*)&ind, sizeInt); // буфер индексов
				if (!inputFile)
					return false;
			}
		}
	}
	printMessageToLog("Write meshes:  " + to_string(meshesName.size()) + ". Successfull.");
	printMessageToLog("Write indeces: " + to_string(allIndexs) + ". Successfull.");
	return true;
}

//////////////////////////////////////////////////////////////

bool pObjectPlugin::writeOptionsFromFile(const MString& mname, const MString & option)
{
	// настройки
	MString pso; // pipline state

	int block = pObjFormatBlocks::optionsInfo;
	inputFile.write((char*)&block, sizeInt); // пишем сначала блок(главный)
	if (!inputFile)
	{
		MStreamUtils::stdErrorStream() << "Error write data from " << mname.asChar() << " file.\n";
		return false;
	}
	if (option.length() > 0) // настройки есть
	{
		MStringArray opt;
		option.split(L';', opt); 
		for(int i(0);i<opt.length();i++)
		{
			MStringArray currentOpt;
			opt[i].split(L'=', currentOpt);
			if (currentOpt[0] == "pso")
				pso = currentOpt[1];
		}
	}
	else
	{
		MStreamUtils::stdErrorStream() << "Error read option for export!\n";
		return false;
	}
	// пишем в файл
	if (!writeLineFromFile(pso.asChar())) // пишем название материала
		return false;

	string out = "Write options model: successfull.";
	printMessageToLog(out);
	return true;
}

//////////////////////////////////////////////////////////////
void pObjectPlugin::clear()
{
	bufMaterials.clear(); // буфер текстур и материалов
	arrayIndeces.clear(); // буфер индексов
	arrayVertex.clear(); // массив вершин для этой модели, после конструирования записывается в файл
	arrayNormal.clear(); // массив нормалей пока не общих
	tmpIndecesMaterialBuffer.clear(); // временный буфер индексов материалов
	meshesName.clear(); // имена мешей
	meshesIndeces.clear(); // индексные буферы для конкретного меша
	inputFile.close(); // закрываем файл
}

void pObjectPlugin::printMessageToLog(const string& m)
{
	string message = "scrollField -e -it \"";
	message += m.c_str();
	message += "\\n\"";
	message += nameLogVar.c_str();
	message += ";";
	CHECK_MSTATUS(MGlobal::executeCommand(message.c_str()));
}

void pObjectPlugin::initProperties()
{
	props =
	{
		{"$game_path", ""},
		{"$version_plugin", ""},
		{"$vendor_plugin", ""},
		{"$name_plugin", ""},
		{"$game_textures", ""}
	};
	if (!Utils::loadConfig(props))
	{
		isInit = false;
		return;
	}
}

//////////////////////////////////////////////////////////////
