#include "progressivePlugin.h"

static MString nameLoadPluginScript("pEngineLoadPlugin.mel");

MStatus initializePlugin(MObject obj)
{
	// load scripts pEngineTools
	MString command;
	CHECK_MSTATUS(MGlobal::executeCommand("internalVar-usd", command));
	command += "pEnginePlugin/" + nameLoadPluginScript + "\"";
	command = "source \"" + command;
	CHECK_MSTATUS(MGlobal::executeCommand(command));
	CHECK_MSTATUS(MGlobal::executeCommand("pEngineLoadPlugin();"));

	// load plugin pEngine
	pObjectPlugin::initProperties();
	if (!pObjectPlugin::isInit)
		return MStatus::kFailure;
	MFnPlugin plugin(obj, pObjectPlugin::props["$vendor_plugin"].c_str(), pObjectPlugin::props["$version_plugin"].c_str(), "Any"); // инициализируем плагин: объект, пришедший из Maya, имя поставщика, версия, версия API maya
	
	// регистрируем транслятор нового файла
	MStatus result = plugin.registerFileTranslator(pObjectPlugin::props["$name_plugin"].c_str(),  // что будет отображаться в имени формата
		"none", // путь к значку
		pObjectPlugin::creator, // функция, которая конструирует новый файл
		NULL, // mel - скрипт, который отображает некие параметры сохранения файла в диалогоом окне сохранения
		pObjectPlugin::optionScriptDefaultValues); // значение параметров по - умолчанию, которое будет передано диалоговому окну            
	if (result != MStatus::kSuccess)
		return result;

	// регистрируем новый материал
	return pEngineMaterial::initMaterial(plugin, pObjectPlugin::props["$game_path"]);
}
//////////////////////////////////////////////////////////////

MStatus uninitializePlugin(MObject obj)
{
	MString command;
	CHECK_MSTATUS(MGlobal::executeCommand("internalVar-usd", command));
	command += "pEnginePlugin/" + nameLoadPluginScript + "\"";
	command = "source \"" + command;
	CHECK_MSTATUS(MGlobal::executeCommand(command));
	CHECK_MSTATUS(MGlobal::executeCommand("pEngineUnloadGui();"));

	MFnPlugin plugin(obj);
	MStatus result = plugin.deregisterFileTranslator(pObjectPlugin::props["$name_plugin"].c_str());
	if (result != MStatus::kSuccess)
		return result;

	return pEngineMaterial::unInitMaterial(plugin);
}