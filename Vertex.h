#pragma once

#include "Utils.h"

using namespace std;

class VertexComparatorFull
{
public:
	bool operator()(const Vertex& v1, const Vertex& v2)const;
};

class VertexComparator
{
public:
	bool operator()(const Vertex& v1, const Vertex& v2)const;
};

struct Material
{
	string nameMaterial;
	string nameTexture;
	string valueMaterial;
	bool doubleSides;
};

class MaterialComparator
{
public:
	bool operator()(const Material& m1, const Material & m2)const;

};

struct Trio
{
	void* first;
	int second;
	bool third;

	Trio(void* f, const int&s, const bool& t = true):first(f), second(s), third(t){}
};

struct Normal
{
	Vector3 normal;
	unsigned int count;

	Normal():normal(), count(0){}
	Normal(const Vector3& v, unsigned int c):normal(v), count(c){}
};