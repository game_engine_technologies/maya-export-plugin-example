# Progressive engine. Autodesk Maya export plugin example

- support export triangles;
- supports loading engine materials/PSO;
- create bbox;
- supports materials;
- support multi meshes;
- supports indeceds and vertexced;
- support dsides;
- supports textures/colors.

### Not for commercial use only
