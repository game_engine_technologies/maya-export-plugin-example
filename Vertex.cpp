#include "Vertex.h"

bool VertexComparatorFull::operator()(const  Vertex& v1, const Vertex& v2)const
{
	// Сравнить все компоненты(позиция, текстурные координаты)
	// TODO: протестировать данный метод

	Vector3 pos1 = v1.pos;
	Vector3 pos2 = v2.pos;
	Vector4 col1 = v1.color;
	Vector4 col2 = v2.color;

	if (pos1 < pos2)
		return true;
	else if (pos1 > pos2)
		return false;
	if (col1 < col2)
		return true;

	return false;
}

bool VertexComparator::operator()(const  Vertex& v1, const Vertex& v2)const
{
	// Сравнить компоненты(позиция)
	Vector3 pos1 = v1.pos;
	Vector3 pos2 = v2.pos;
	if (pos1 < pos2)
		return true;
	return false;
}

//////////////////////////////////////////////////////////////
bool MaterialComparator::operator()(const Material& m1, const Material& m2) const
{
	if (m1.nameTexture < m2.nameTexture)
		return true;
	else if (m1.nameTexture > m2.nameTexture)
		return false;
	if (m1.valueMaterial < m2.valueMaterial)
			return true;
	return false;
}
