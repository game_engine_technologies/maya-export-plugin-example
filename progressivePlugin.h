#pragma once
// Range, Progressive Engine 2018

#include "progressiveMaterial.h"

#if defined (_WIN32)
#define strcasecmp _stricmp
#endif

#define NO_SMOOTHING_GROUP      -1
#define INITIALIZE_SMOOTHING    -2
#define INVALID_ID              -1


//////////////////////////////////////////////////////////////
class pObjectPlugin : public MPxFileTranslator
{
	// члены
	ofstream inputFile; // выходной файл
	map<Material, int, MaterialComparator> bufMaterials; // буфер материалов, int - порядковый номер материала, заодно и номер массива индексов в буфере
	map<int, vector<Index>> arrayIndeces; // буфер с буферами индексов, ключ - индекс материала
	map <Vertex, int, VertexComparatorFull > arrayVertex; // массив вершин для этой модели, после конструирования записывается в файл
	map<Vertex, set<Vector3>, VertexComparator> arrayNormal; // массив нормалей, пока не общих (для усреднения)
	vector<int> tmpIndecesMaterialBuffer; // временный буфер индексов материалов
	string nameLogVar; // имя переменной лога на стороне mel - скриптов
	vector<string> meshesName; // буфер мешей, имя
	vector<int> meshesIndeces; // буфер мешей, - кол-во индексных буферов
	string pExt; // расширение формата

public:
	// static члены
	static const char* const optionScript; // скрипт опций при экспорте
	static const char* const optionScriptDefaultValues; // скрипт настроек по умолчанию
	static map<string, string> props; // файл с настройками
	static bool isInit; // успешно ли загружен плагин

	// конструкторы
	pObjectPlugin();
	static void* creator(); // создаём экземпляр класса

	// деструкторы
	virtual ~pObjectPlugin() {};

	// методы
	MStatus reader(const MFileObject& file,
		const MString& optionsString,
		FileAccessMode mode);

	MStatus writer(const MFileObject& file,
		const MString& optionsString,
		FileAccessMode mode);
	bool haveReadMethod() const;
	bool haveWriteMethod() const;
	MString defaultExtension() const;
	MFileKind identifyFile(const MFileObject& fileName,
		const char* buffer,
		short size) const;





	MFnDagNode get_object_by_name(const char* name);
	static void initProperties(); // инициализация настроек

private:
	MStatus OutputPolygons(MDagPath&); // построение полигонов
	MStatus exportSelection(); // экспорт выделенных моделей
	MStatus exportAll(); // экспорт всех моделей
	bool isVisible(MFnDagNode& fnDag, MStatus& status); // скрыт или показан объект на сцене
	MStatus writePolyFromBuffer(MFnMesh& fnMesh,
		MDagPath& mdagPath,
		MObject& set,
		MObject& comp); // пишем полигоны в буфер
	void setToLongUnitName(const MDistance::Unit&, MString&); // узнаем единицы измерения в майке
	bool writeInfoFromFile(); // пишем всю начальную информацию в файл
	bool writeMaterialsFromFile(); // пишем все текстуры в буфер
	bool writeLineFromFile(const char* line); // запись строки в буфер
	bool writeVertexsFromFile(); // запись вершин в буфер
	bool writeMeshesFromFile(); // запись всех мешей в буфер
	bool writeOptionsFromFile(const MString& mname, const MString& option); // запись 
	void clear(); // очищаем все буферы перед экспортом
	void printMessageToLog(const string& m); // печать в лог с помощью mel

};