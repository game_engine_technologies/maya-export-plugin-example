#define WIN32_LEAN_AND_MEAN
#pragma once
#define _MApiVersion
#define NOMINMAX

// windows
#include <Windows.h>

// maya
#include <sys/types.h>
#include <maya/MStatus.h>
#include <maya/MPxCommand.h>
#include <maya/MString.h>
#include <maya/MStringArray.h>
#include <maya/MArgList.h>
#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>
#include <maya/MItSelectionList.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MDagPath.h>
#include <maya/MDagPathArray.h>
#include <maya/MFnPlugin.h>
#include <maya/MFnMesh.h>
#include <maya/MFnSet.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatArray.h>
#include <maya/MObjectArray.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MPxFileTranslator.h>
#include <maya/MFnDagNode.h>
#include <maya/MItDag.h>
#include <maya/MDistance.h>
#include <maya/MIntArray.h>
#include <maya/MIOStream.h>
#include <maya/MBoundingBox.h>
#include <maya/MGlobal.h> 
#include <maya/MStreamUtils.h>
#include <maya/MPlugArray.h>
#include <maya/MItDependencyGraph.h>
#include <maya/MFnRenderLayer.h>
#include <maya/MViewport2Renderer.h>
#include <maya/MImage.h>
#include <maya/M3dView.h>
#include <maya/MrenderView.h>
#include <maya/MDagModifier.h>
#include <maya/MFnTransform.h>
#include <maya/MFnCamera.h>
#include <maya/MFnDagNode.h>
#include <maya/MEulerRotation.h>
#include <maya/MDrawRegistry.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnLightDataAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MDataBlock.h>
#include <maya/MPlug.h>

// c++
#include <fstream>
#include <time.h>
#include <sstream>
#include <iomanip>
#include <vector>
#include <set>
#include <algorithm>
#include <iterator>
#include <map>

// engine
#include "../pEngineStatic/loaders.h"

using namespace gmath;
using namespace std;

class Utils
{
public:
	static bool loadConfig(map<string, string>& props);
	static bool loadMelScript(string& script);
};
